\contentsline {chapter}{\numberline {1}Basic Concepts}{1}{chapter.1}%
\contentsline {section}{The Centimetre and the Second}{1}{section*.2}%
\contentsline {section}{Weight and Mass}{6}{section*.3}%
\contentsline {section}{The SI System and Standards of Measurement}{10}{section*.7}%
\contentsline {section}{Density}{13}{section*.8}%
\contentsline {section}{The Law of Conservation of Mass}{17}{section*.9}%
\contentsline {section}{Action and Reaction}{18}{section*.10}%
\contentsline {section}{How Velocities Are Added}{20}{section*.11}%
\contentsline {section}{Force Is a Vector}{24}{section*.15}%
\contentsline {section}{Inclined Plane}{29}{section*.20}%
\contentsline {chapter}{\numberline {2}Laws of Motion}{33}{chapter.2}%
\contentsline {section}{Various Points of View About Motion}{33}{section*.22}%
\contentsline {section}{The Law of Inertia}{35}{section*.23}%
\contentsline {section}{Motion Is Relative}{38}{section*.24}%
\contentsline {section}{The Point of View of a Celestial Observer}{41}{section*.25}%
\contentsline {section}{Acceleration and Force}{44}{section*.27}%
\contentsline {section}{Rectilinear Motion with Constant Acceleration}{52}{section*.29}%
\contentsline {section}{Path of a Bullet}{55}{section*.30}%
\contentsline {section}{Circular Motion}{59}{section*.33}%
\contentsline {section}{Life at \emph {g} Zero}{63}{section*.36}%
\contentsline {section}{Motion from an ``Unreasonable'' Point of View}{69}{section*.38}%
\contentsline {section}{Centrifugal Forces}{74}{section*.42}%
\contentsline {section}{Coriolis Forces}{81}{section*.46}%
\contentsline {chapter}{\numberline {3}Conservation Laws}{91}{chapter.3}%
\contentsline {section}{Recoil}{91}{section*.51}%
\contentsline {section}{The Law of Conservation of Momentum}{93}{section*.53}%
\contentsline {section}{Jet Propulsion}{97}{section*.55}%
\contentsline {section}{Motion Under the Action of Gravity}{101}{section*.57}%
\contentsline {section}{The Law of Conservation of Mechanical Energy}{106}{section*.60}%
\contentsline {section}{Work}{110}{section*.61}%
\contentsline {section}{In What Units Work and Energy Are Measured}{113}{section*.63}%
\contentsline {section}{Power and Efficiency of Machines}{114}{section*.64}%
\contentsline {section}{Energy Loss}{116}{section*.65}%
\contentsline {section}{Perpetuum Mobile}{117}{section*.66}%
\contentsline {section}{Collisions}{122}{section*.71}%
\contentsline {chapter}{\numberline {4}Oscillations}{125}{chapter.4}%
\contentsline {section}{Equilibrium}{125}{section*.73}%
\contentsline {section}{Simple Oscillations}{127}{section*.75}%
\contentsline {section}{Displaying Oscillations}{132}{section*.77}%
\contentsline {section}{Force and Potential Energy in Oscillations}{137}{section*.80}%
\contentsline {section}{Spring Vibrations}{140}{section*.82}%
\contentsline {section}{More Complex Oscillations}{144}{section*.84}%
\contentsline {section}{Resonance}{145}{section*.86}%
\contentsline {chapter}{\numberline {5}Motion of Solid Bodies}{149}{chapter.5}%
\contentsline {section}{Torque}{149}{section*.88}%
\contentsline {section}{Lever}{153}{section*.91}%
\contentsline {section}{Loss in Path}{156}{section*.94}%
\contentsline {section}{Other Very Simple Machines}{160}{section*.95}%
\contentsline {section}{Adding Parallel Forces Acting on a Solid Body}{162}{section*.97}%
\contentsline {section}{Centre of Gravity}{166}{section*.101}%
\contentsline {section}{Centre of Mass}{171}{section*.106}%
\contentsline {section}{Angular Momentum}{174}{section*.107}%
\contentsline {section}{Law of Conservation of Angular Momentum}{175}{section*.109}%
\contentsline {section}{Angular Momentum as a Vector}{178}{section*.110}%
\contentsline {section}{Tops}{180}{section*.112}%
\contentsline {section}{Flexible Shaft}{183}{section*.114}%
\contentsline {chapter}{\numberline {6}Gravitation}{187}{chapter.6}%
\contentsline {section}{What Holds the Earth Up!}{187}{section*.115}%
\contentsline {section}{Law of Universal Gravitation}{189}{section*.116}%
\contentsline {section}{Weighing the Earth}{192}{section*.118}%
\contentsline {section}{Measuring \emph {g} in the Service of Prospecting}{194}{section*.119}%
\contentsline {section}{Weight Underground}{199}{section*.122}%
\contentsline {section}{Gravitational Energy}{201}{section*.124}%
\contentsline {section}{How Planets Move}{206}{section*.125}%
\contentsline {section}{Interplanetary Travel}{213}{section*.130}%
\contentsline {section}{If There Were No Moon}{216}{section*.132}%
\contentsline {chapter}{\numberline {7}Pressure}{225}{chapter.7}%
\contentsline {section}{Hydraulic Press}{225}{section*.136}%
\contentsline {section}{Hydrostatic Pressure}{227}{section*.138}%
\contentsline {section}{Atmospheric Pressure}{231}{section*.141}%
\contentsline {section}{How Atmospheric Pressure Was Discovered}{235}{section*.144}%
\contentsline {section}{Atmospheric Pressure and Weather}{237}{section*.145}%
\contentsline {section}{Change of Pressure with Altitude}{240}{section*.147}%
\contentsline {section}{Archimedes' Principle}{242}{section*.148}%
\contentsline {section}{Extremely Low Pressures. Vacuum}{248}{section*.151}%
\contentsline {section}{Pressures of Millions of Atmospheres}{250}{section*.152}%
